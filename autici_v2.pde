// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float pedala = 0;
}

// Moram imati klasu koja prestavlja merni rezultat pod nazivom Measurement
class Measurement
{
  float brzina = 0,
        rastojanje = 0;
}

// Moja simulacija mora da nasledi klasu Simulation i da implementira
// metode Update, Measure, Control, Visualize
class MojaSimulacija extends Simulation
{
  float ogranicenje = 40,
        ubrzanje = 0.05,
        optimalno_rastojanje = 30,
        brzina = 0,
        vreme = 1136,
        lokacija = 0,
        lokacija_npc = 100,
        pedala = 0;
        
  boolean preticanje = true, preticao = false;
  char traka = 'd';
  float autoPosY = 80;
        
  float vreme_koraka = GetUpdatePeriod();

  MojaSimulacija ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(4);
    
    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(2);
    
  }

  // Napravim korak simulacije. Npr. uradim numericku integraciju diferencijalne jednacine.
  // Dato mi je i upravljanje koje je izgenerisao kontroler.
  void Update(Actuation i)
  {
    //brzina += ubrzanje * vreme_koraka * i.pedala;
    brzina = ((-brzina + ogranicenje * i.pedala) * vreme_koraka) / ubrzanje + brzina;
    brzina= round(brzina * 100) / 100;
    lokacija += brzina * vreme_koraka;
    if (lokacija + 16 >= lokacija_npc) {
      if (traka == 'd' && !preticao) {
        lokacija = lokacija_npc - 16;
        brzina = -brzina * 2;
        i.pedala = -1;
      } else if (lokacija - 16 >= lokacija_npc) {
        preticao = true;
      } else if (lokacija - 16 < lokacija_npc) {
        if (traka == 'l')
          preticao = false;
        else {
          lokacija = lokacija_npc + 16;
          i.pedala = 1;
          brzina *= 5;
        }
      }
    }
  }

  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.brzina = 0;
    lokacija_npc = mouseX / 5;
    m.rastojanje = lokacija_npc - lokacija;
    if (preticanje) {
      if (mouseY < 80) {
        traka = 'l';
        if (autoPosY > 40)
          autoPosY -= 1;
      } else {
        if (lokacija > lokacija_npc + 16 || lokacija < lokacija_npc - 16) {
          traka = 'd';
          if (autoPosY < 80)
            autoPosY += 1;
        }
      }
    }
    return m;
  }

  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    Actuation o = new Actuation();
    if (traka == 'd' && !preticao)
      o.pedala = (m.rastojanje - optimalno_rastojanje) * 0.1;
    else {
      o.pedala = 1;
    }
    if (o.pedala > 1)
      o.pedala = 1;
    else if (o.pedala < -1)
      o.pedala = -1;
    pedala = o.pedala;
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {
    background(bck);
    println("Brzina: " + brzina / 0.277 + " km/h");
    image(a1, lokacija * 5, autoPosY, 80, 40);
    image(a2, lokacija_npc * 5, 80, 80, 40);
    
    fill(0);
    rect(5, 5, 5, 150);
    fill(255, 0, 0);
    rect(5, 78 - brzina * 1.5, 5, 4);
    fill(255);
    rect(3, 75 - pedala * 75, 9, 5);
    if (vreme == 1136) {
      vreme = 0;
      clear();  
    }
    vreme++;
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(1136, 160);
  sim = new MojaSimulacija();
}